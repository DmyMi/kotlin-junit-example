package demo

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.util.Arrays


@RunWith(Parameterized::class)
class DataDriven(var a: Int, var b: Int, var result: Int) {

    companion object {
        @Parameterized.Parameters
        @JvmStatic
        fun data(): Collection<Array<Any>> {
            val data = arrayOf(arrayOf<Any>(2, 2, 4), arrayOf<Any>(5, -3, 2), arrayOf<Any>(121, 4, 125))
            return Arrays.asList(*data)
        }
    }

    @Test
    fun letsTest() {
        Assert.assertEquals(result, add(a, b))
    }
}