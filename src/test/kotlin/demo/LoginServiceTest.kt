package demo

import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

class LoginServiceTest {
    @Test
    fun testItShouldSetAccountToLoggedInWhenPasswordMatches() {
        val account = mock(IAccount::class.java)
        `when`(account.passwordMatches(ArgumentMatchers.anyString())).thenReturn(true)

        val accountRepository = mock(IAccountRepository::class.java)
        `when`(accountRepository.find(ArgumentMatchers.anyString())).thenReturn(account)

        val service = LoginService(accountRepository)

        service.login("admin", "password")

        verify(account, times(1)).setLoggedIn(true)
    }

    @Test
    fun testItShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
        val account = mock(IAccount::class.java)
        `when`(account.passwordMatches(ArgumentMatchers.anyString())).thenReturn(false)

        val accountRepository = mock(IAccountRepository::class.java)
        `when`(accountRepository.find(ArgumentMatchers.anyString())).thenReturn(account)

        val service = LoginService(accountRepository)
        for (i in 0..2)
            service.login("admin", "password")

        verify(account, times(1)).setRevoked(true)
    }

    @Test
    fun testItShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
        val account = mock(IAccount::class.java)
        `when`(account.passwordMatches(ArgumentMatchers.anyString())).thenReturn(false)

        val accountRepository = mock(IAccountRepository::class.java)
        `when`(accountRepository.find(ArgumentMatchers.anyString())).thenReturn(account)

        val service = LoginService(accountRepository)

        service.login("admin", "password")
        verify(account, never()).setLoggedIn(true)
    }


}
