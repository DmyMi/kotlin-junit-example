package demo

interface IAccountRepository {
    fun find(accountId: String): IAccount
}
