package demo

class LoginService(private val accountRepository: IAccountRepository) {
    private var failedAttempts = 0

    init {
        println("Instantiated the repository: $accountRepository")
    }

    fun login(accountId: String, password: String) {
        println("Login started: User: $accountId, password: $password")

        val account = accountRepository.find(accountId)
        println("Got account from repo: $account")

        if (account.passwordMatches(password)) {
            account.setLoggedIn(true)
            println("Login success")
        } else {
            ++failedAttempts
            println("$failedAttempts failed attemps")
        }


        if (failedAttempts == 3) {
            account.setRevoked(true)
            println("Account revoked")
        }


    }
}
