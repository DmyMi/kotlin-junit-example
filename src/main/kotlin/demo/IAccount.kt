package demo


interface IAccount {
    fun setLoggedIn(value: Boolean)
    fun passwordMatches(candidate: String): Boolean
    fun setRevoked(value: Boolean)
}

